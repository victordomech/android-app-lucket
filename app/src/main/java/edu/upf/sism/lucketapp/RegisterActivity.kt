package edu.upf.sism.lucketapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class RegisterActivity : AppCompatActivity() {

    private lateinit var txtName:EditText
    private lateinit var txtSurname:EditText
    private lateinit var txtEmail:EditText
    private lateinit var txtPassword:EditText
    private lateinit var progressBar: ProgressBar
    private lateinit var dbReference:DatabaseReference
    private lateinit var database:FirebaseDatabase
    private lateinit var auth:FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        txtName= findViewById(R.id.txtRegName)
        txtSurname= findViewById(R.id.txtRegSurname)
        txtEmail= findViewById(R.id.txtRegMail)
        txtPassword= findViewById(R.id.txtRegPass)

        progressBar= findViewById(R.id.ProgressBarReg)

        database = FirebaseDatabase.getInstance()
        auth= FirebaseAuth.getInstance()
        dbReference= database.reference.child("User")
    }

    fun register(view:View){

        createNewAccount()
    }

    private fun createNewAccount(){

        val name:String= txtName.text.toString()
        val surname:String= txtSurname.text.toString()
        val mail:String= txtEmail.text.toString()
        val password:String= txtPassword.text.toString()

        if(!TextUtils.isEmpty(name)&&!TextUtils.isEmpty(surname)&&!TextUtils.isEmpty(mail)&&!TextUtils.isEmpty(password)){

            progressBar.visibility= View.VISIBLE
            auth.createUserWithEmailAndPassword(mail,password)
                .addOnCompleteListener(this) {
                    task->
                    if(task.isComplete){

                        val user: FirebaseUser?= auth.currentUser
                        verifyEmail(user)

                        val userDB= dbReference.child(user!!.uid)
                        userDB.child("Name").setValue(name)
                        userDB.child("Surname").setValue(surname)
                        action()
                    }
                }
        }
    }

    private fun action(){

        startActivity(Intent(this, LoginActivity::class.java))
    }

    private fun verifyEmail(user:FirebaseUser?){
        user?.sendEmailVerification()?.addOnCompleteListener(this){

            task->
            if(task.isComplete){

                Toast.makeText(this, "Email enviado", Toast.LENGTH_LONG).show()
            }
            else{

                Toast.makeText(this, "Error al enviar el email", Toast.LENGTH_LONG).show()
            }
        }
    }
}
