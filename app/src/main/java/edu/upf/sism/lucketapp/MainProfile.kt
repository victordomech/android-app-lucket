package edu.upf.sism.lucketapp

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import edu.upf.sism.lucketapp.models.TicketPost
import kotlinx.android.synthetic.main.activity_main.*


class MainProfile : AppCompatActivity() {

    private lateinit var ticketAdapter: TicketRecyclerAdapter
    private lateinit var auth: FirebaseAuth
    private lateinit var dbReference: DatabaseReference
    private lateinit var database: FirebaseDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        auth= FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance()
        dbReference= database.reference.child("User").child(auth.getCurrentUser()?.getUid().toString()).child("Tickets")
        initRecyclerView()
        addTicketData()
    }

    fun addBoleto(view: View){
        startActivity(Intent(this, NewTicketActivity::class.java))
    }

    fun signOut(view: View){
        auth.signOut()
        startActivity(Intent(this, LoginActivity::class.java))
    }

    private fun createListOfTickets(): ArrayList<TicketPost>{
        val list = ArrayList<TicketPost>()

        val postListener = object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (noteSnapshot in dataSnapshot.children) {
                    val ticket: TicketPost? = noteSnapshot.getValue<TicketPost>(TicketPost::class.java)
                    if (ticket != null) {
                        list.add(ticket)
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Toast.makeText(baseContext, "Failed to load ticket.", Toast.LENGTH_SHORT).show()
            }
        }
        dbReference.addValueEventListener(postListener)

        return list
    }

    private fun validateTicket(): ArrayList<TicketPost>{

        val list = ArrayList<TicketPost>()

        return list
    }

    private fun addTicketData(){

        val data = createListOfTickets()
        ticketAdapter.submitList(data)
    }


    private fun initRecyclerView(){

        recycler_view.apply {
            layoutManager = LinearLayoutManager(this@MainProfile)
            ticketAdapter = TicketRecyclerAdapter()
            adapter = ticketAdapter
        }
    }

}
