package edu.upf.sism.lucketapp

import android.content.Intent
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.VisibleForTesting
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.layout_single_ticket.view.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import edu.upf.sism.lucketapp.models.TicketPost
import kotlin.collections.ArrayList


class TicketRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>()
{


    private val TAG: String = "AppDebug"

    private var items: List<TicketPost> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.layout_single_ticket, parent, false)
        return TicketViewHolder(view).listen{ pos, type ->
            val item = items.get(pos)
            item.validate()
            println("*****************************"+item)
        }
        /*return TicketViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.layout_single_ticket, parent, false)
        )*/
    }

    fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener {
            event.invoke(getAdapterPosition(), getItemViewType())
        }

        return this
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder) {

            is TicketViewHolder -> {
                holder.bind(items.get(position))
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun submitList(TicketList: List<TicketPost>){
        items = TicketList
    }

    class TicketViewHolder
    constructor(
        itemView: View
    ): RecyclerView.ViewHolder(itemView){

        val ticket_image = itemView.imgProfileType
        val ticket_date = itemView.txtProfileDate
        val ticket_number = itemView.txtProfileNumber
        val ticket_contribution = itemView.txtProfileContribution
        val ticket_description = itemView.txtProfileDescription
        val ticket_prize = itemView.txtProfilePrize
        val ticket_button = itemView.buttonProfileSubmit
        var imgURL =""
        var prizeText =""


        fun bind(ticketPost: TicketPost){

            if(ticketPost.type=="Lotería Nacional"){
                imgURL ="https://s1.eestatic.com/2019/12/26/loterias/Loterias-Sorteos_deportivos-Loterias_454966347_141248383_1706x960.jpg"
            }
            else if(ticketPost.type=="Euromillones"){
                imgURL ="https://estaticos.elperiodico.com/resources/jpg/2/8/resultados-del-euromillones-1489236641882.jpg"
            }
            else if(ticketPost.type=="La Primitiva"){
                imgURL ="https://fotografias.antena3.com/clipping/cmsimages02/2019/09/26/B01CFB09-3D32-42B0-B549-6194F79A81BF/58.jpg"
            }
            else if(ticketPost.type=="El Gordo"){
                imgURL ="https://fotografias.antena3.com/clipping/cmsimages02/2019/09/29/AC60F475-CC3D-4A7D-BE34-C6B17C0DEA1C/58.jpg"
            }
            else if(ticketPost.type=="Bonoloto"){
                imgURL ="https://fotografias.antena3.com/clipping/cmsimages02/2019/09/25/86F365C0-8779-4542-87BB-BC416C5923BE/58.jpg"
            }

            if(ticketPost.prize=="-1"){
                prizeText = "SORTEO NO REALIZADO"
                ticket_button.visibility = View.VISIBLE
            }
            else if(ticketPost.prize=="0"){
                prizeText = "NUMERO NO PREMIADO"
                ticket_prize.setTextColor(Color.parseColor("#FFE91E63"))
            }
            else{
                prizeText = "NUMERO PREMIADO CON "+ticketPost.prize + "€"
                ticket_prize.setTextColor(Color.parseColor("#FF4CAF50"))
            }

            val requestOptions = RequestOptions()
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)

            Glide.with(itemView.context)
                .applyDefaultRequestOptions(requestOptions)
                .load(imgURL)
                .into(ticket_image)
            ticket_date.setText(ticketPost.date)
            ticket_number.setText(ticketPost.num)
            ticket_contribution.setText(ticketPost.contribution + " €")
            ticket_description.setText(ticketPost.desc)
            ticket_prize.setText(prizeText)
        }

    }

}