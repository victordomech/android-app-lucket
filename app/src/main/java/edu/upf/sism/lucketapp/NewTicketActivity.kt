package edu.upf.sism.lucketapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Spinner
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase



data class Ticket(
    val type: String,
    val date: String,
    var num: String,
    val contribution: String,
    val desc: String,
    val prize: String
)

class NewTicketActivity : AppCompatActivity() {

    private lateinit var txtType: Spinner
    private lateinit var txtDate: EditText
    private lateinit var txtNum: EditText
    private lateinit var txtContr: EditText
    private lateinit var txtDesc: EditText
    private lateinit var progressBar: ProgressBar
    private lateinit var auth: FirebaseAuth
    private lateinit var dbReference: DatabaseReference
    private lateinit var database: FirebaseDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_bono)

        txtType= findViewById(R.id.txtNewBonoType)
        txtDate= findViewById(R.id.txtNewBonoDate)
        txtNum= findViewById(R.id.txtNewBonoNum)
        txtDesc= findViewById(R.id.txtNewBonoDesc)
        txtContr= findViewById(R.id.txtNewBonoContribution)

        progressBar= findViewById(R.id.ProgressBarAddBono)

        database = FirebaseDatabase.getInstance()
        auth= FirebaseAuth.getInstance()
        dbReference= database.reference.child("User")
    }

    fun add(view: View){
        addBoletos()
    }

    fun before(view: View){
        startActivity(Intent(this, MainProfile::class.java))
    }

    private fun addBoletos(){

        val type:String= txtType.selectedItem.toString()
        val date:String= txtDate.text.toString()
        val num:String= txtNum.text.toString()
        val contribution:String= txtContr.text.toString()
        val desc:String= txtDesc.text.toString()
        val prize:String= "-1"
        val user: FirebaseUser?= auth.currentUser

        if(!TextUtils.isEmpty(type)&&!TextUtils.isEmpty(date)&&!TextUtils.isEmpty(num)){

            progressBar.visibility= View.VISIBLE

            if (user != null) {

                val userDB= dbReference.child(auth.getCurrentUser()?.getUid().toString())

                val myTicket = Ticket(type,date,num,contribution,desc,prize)
                val keynum = userDB.child("Tickets").push().key
                keynum?.let { userDB.child("Tickets").child(it).setValue(myTicket) }
                action()

            }
        }
    }

    private fun action(){
        startActivity(Intent(this, MainProfile::class.java))
    }

}