package edu.upf.sism.lucketapp.models
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*

class TicketPost(

    public var type: String,
    public var date: String,
    public var num: String,
    public var contribution: String,
    public var desc: String,
    public var prize: String

) {
    constructor() : this("", "",
    "", "", "",
    ""
    )


    override fun toString(): String {
        return "TicketPost(type='$type', date='$date', num='$num', contribution='$contribution', desc='$desc', prize='$prize')"
    }

    fun validate(){

        var index = num.get(0)
        if(index=='1'&&index=='5'){

            prize= "100"
        }
        else if(index=='0'&&index=='4'){

            prize= "5000"
        }

        else{
            prize= "0"
        }
    }
}